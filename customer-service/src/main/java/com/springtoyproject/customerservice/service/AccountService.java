package com.springtoyproject.customerservice.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("account-service")
public interface AccountService {
    @GetMapping("/accounts/numberOfAccounts")
    Integer getNumberOfAccounts();
}
