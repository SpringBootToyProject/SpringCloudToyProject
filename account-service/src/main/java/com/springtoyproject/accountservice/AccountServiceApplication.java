package com.springtoyproject.accountservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class AccountServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountServiceApplication.class, args);
    }

    @Autowired
    private Environment environment;

    @GetMapping("/accounts/numberOfAccounts")
    public Integer getListAccounts()
    {
        System.out.println("Account-service, port: " + environment.getProperty("local.server.port"));
        return Integer.valueOf(environment.getProperty("local.server.port"));
    }
}
